### Make sure to have NodeJS installed ###

1. Install dependencies:
  * Run "npm install -d" in root folder (installs all dependencies from _package.json_ and _bower.json_, and then runs _grunt build_)

2. Start server:
  + GUI(Windows only):
    * Execute start-server.bat
  + Command Line:
    * Run "node chatserver.js" in root folder

3. Start client:
  + GUI(Windows only):
    * Execute start-client.bat
  + Command Line:
    * Run "python -m http.server 8000" in root folder (You will have to have python in your PATH)

4. Open app:
  * Open web browser and enter URL [http://localhost:8000/](http://localhost:8000/)

- - - -

### EDITs to chatserver.js ###

1. Line 151: CHANGED name of '__disconnect__' event listener to '__disconnectuser__'

2. Line 238/250: In '__ban__' and '__unban__' ADDED _banned_ parameter to '__updateusers__' event-emit

3. Line 274: ADDED '__majorfix__' event listener to update roompage when a user enters his/her first room