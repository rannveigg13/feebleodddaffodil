'use strict';

var ChatClient = angular.module('ChatClient', ['ngRoute']);

ChatClient.config(['$routeProvider',
	function ($routeProvider) {
		$routeProvider
			.when('/login', { templateUrl: 'views/login.html', controller: 'LoginController' })
			.when('/rooms/:user/', { templateUrl: 'views/rooms.html', controller: 'RoomsController' })
			.when('/room/:user/:room/', { templateUrl: 'views/room.html', controller: 'RoomController' })
			.when('/disconnect', { templateUrl: 'views/tmp.html', controller: 'DisconnectController'})
			.otherwise({
	  			redirectTo: '/login'
			});
	}
]);