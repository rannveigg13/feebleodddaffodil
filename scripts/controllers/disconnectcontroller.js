(function () {
	'use strict';

	angular.module('ChatClient').controller('DisconnectController', 
	['$scope', '$location', '$rootScope', '$routeParams', 'socket',
	function ($scope, $location, $rootScope, $routeParams, socket) {
		socket.emit('disconnectuser');
		socket.on('servermessage', function (action, room, user) {
			if (action === 'quit') {
				$location.path('/login');
			}
		});
	}]);
}());