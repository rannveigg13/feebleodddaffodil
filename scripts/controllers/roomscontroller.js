(function () {
	'use strict';

	angular.module('ChatClient').controller('RoomsController', 
	['$scope', '$location', '$rootScope', '$routeParams', 'socket',
	function ($scope, $location, $rootScope, $routeParams, socket) {
		socket.emit('rooms');
		socket.on('roomlist', function (roomList) {
			$scope.rooms = Object.keys(roomList);
		});
		$scope.currentUser = $routeParams.user;
		$scope.roomname = '';
		$scope.passprompt = '';
		$scope.errorMessage = '';
		$scope.roomtried = '';

		$scope.join = function (roomName) {
			var joinRoom = {
				room: roomName,
				pass: $scope.passprompt
			};
			socket.emit('joinroom', joinRoom, function (success, reason) {
				if (success) {
					$location.path('/room/' + $scope.currentUser + "/" + roomName);
				} else {
					if (reason === 'wrong password') {
						$scope.errorMessage = 'Password required: ';
						$scope.roomtried = roomName;
						$scope.passprompt = '';
					} else {
						$scope.errorMessage = reason;
					}
				}
			});
		};

		$scope.create = function () {
			var newRoom = {
				room: $scope.roomname,
				pass: $scope.roompass
			};
			if ($scope.roomname === '') {
				$scope.errorMessage = 'Please choose a room-name before continuing!';
			} else {
				socket.emit('joinroom', newRoom, function (success, reason) {
					if (success) {
						$location.path('/room/' + $scope.currentUser + "/" + $scope.roomname);
					}
				});
			}
		};
	}]);
}());