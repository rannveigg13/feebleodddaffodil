(function () {
	'use strict';

	angular.module('ChatClient').controller('RoomController',
	['$scope', '$location', '$rootScope', '$routeParams', 'socket',
	function ($scope, $location, $rootScope, $routeParams, socket) {
		$scope.currentRoom = $routeParams.room;
		$scope.currentUser = $routeParams.user;
		$scope.currentUsers = [];
		$scope.currentOps = [];
		$scope.currentBanned = [];
		$scope.errorMessage = '';
		$scope.msgtext = '';
		$scope.msgHist = [];
		$scope.privHist = [];
		$scope.currentTopic = '';
		$scope.selectOptions = ['All'];
		$scope.selectedOption = $scope.selectOptions[0];

		var selectlistBuilder = function (users, ops) {
			$scope.selectOptions = ['All'];
			for (var i = 0; i < Object.keys(users).length; i++) {
				var userName = users[Object.keys(users)[i]];
				if (userName !== $scope.currentUser) {
					$scope.selectOptions.push( userName );
				}
			}
			for (var j = 0; j < Object.keys(ops).length; j++) {
				var opName = ops[Object.keys(ops)[j]];
				if (opName !== $scope.currentUser) {
					$scope.selectOptions.push( opName );
				}
			}
		};

		socket.emit('firstroomfix', {room: $scope.currentRoom} );
		socket.on('firstfix', function (userList, opList, banList, msgHistory, topic) {
			$scope.currentUsers = userList;
			$scope.currentOps = opList;
			$scope.currentBanned = banList;
			$scope.msgHist = msgHistory;
			$scope.currentTopic = topic;
			selectlistBuilder(userList, opList);
		});
		socket.on('updateusers', function (roomName, users, ops, banned) {
			if (roomName === $scope.currentRoom) {
				$scope.currentUsers = users;
				$scope.currentOps = ops;
				if (banned !== undefined) {
					$scope.currentBanned = banned;
				}
				selectlistBuilder(users, ops);
			}
		});
		socket.on('updatechat', function (roomName, msgHistory) {
			if (roomName === $scope.currentRoom) {
				$scope.msgHist = msgHistory;
			}
		});
		socket.on('updatetopic', function (roomName, topic, opUser) {
			if (roomName === $scope.currentRoom) {
				$scope.currentTopic = topic;
			}
		});
		socket.on('kicked', function (roomName, kickedUser, opUser) {
			if (roomName === $scope.currentRoom) {
				if ($scope.currentUser === kickedUser) {
					$location.path('/rooms/' + kickedUser);
				}
			}
		});
		socket.on('banned', function (roomName, bannedUser, opUser) {
			if (roomName === $scope.currentRoom) {
				if ($scope.currentUser === bannedUser) {
					$location.path('/rooms/' + bannedUser);
				}
			}
		});
		socket.on('recv_privatemsg', function (sendingUser, msg) {
			$scope.privHist.push({timestamp: new Date(), nick: sendingUser + "->" + $scope.currentUser, message: msg});
		});

		$scope.op = function (person) {
			var obj = {
				room: $scope.currentRoom,
				user: person
			};
			if (obj.user !== $scope.currentUser) {
				socket.emit('op', obj, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to op user';
					}
				});
			} else {
				$scope.errorMessage = 'You can not op yourself!';
			}
		};
		$scope.deop = function (person) {
			var obj = {
				room: $scope.currentRoom,
				user: person
			};
			if (obj.user !== $scope.currentUser) {
				socket.emit('deop', obj, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to deop user';
					}
				});
			} else {
				$scope.errorMessage = 'You can not deop yourself!';
			}
		};
		$scope.kick = function (person) {
			var obj = {
				room: $scope.currentRoom,
				user: person
			};
			if (obj.user !== $scope.currentUser) {
				socket.emit('kick', obj, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to kick user';
					}
				});
			} else {
				$scope.errorMessage = 'You can not kick yourself!';
			}
		};
		$scope.ban = function (person) {
			var obj = {
				room: $scope.currentRoom,
				user: person
			};
			if (obj.user !== $scope.currentUser) {
				socket.emit('ban', obj, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to ban user';
					}
				});
			} else {
				$scope.errorMessage = 'You can not ban yourself!';
			}
		};
		$scope.unban = function (person) {
			var obj = {
				room: $scope.currentRoom,
				user: person
			};
			if (obj.user !== $scope.currentUser) {
				socket.emit('unban', obj, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to unban user';
					}
				});
			} else {
				$scope.errorMessage = 'You can not unban yourself!';
			}
		};

		$scope.leave = function () {
			socket.emit('partroom', $scope.currentRoom);
			$location.path('/rooms/' + $scope.currentUser);
		};

		$scope.send = function () {
			if ($scope.msgtext === '') {
				$scope.errorMessage = 'Please enter something';
			} else {
				var newMsg = {};
				if ($scope.selectedOption === 'All') {
					newMsg.roomName = $scope.currentRoom;
					newMsg.msg = $scope.msgtext;
					socket.emit('sendmsg', newMsg);
					$scope.msgtext = '';
				} else {
					newMsg.nick = $scope.selectedOption;
					newMsg.message = $scope.msgtext;
					socket.emit('privatemsg', newMsg);
					$scope.privHist.push( {timestamp: new Date(), nick: $scope.currentUser + "->" + newMsg.nick, message: newMsg.message} );
					$scope.msgtext = '';
				}
				
			}
		};
		$scope.setTopic = function () {
			if ($scope.msgtext === '') {
				$scope.errorMessage = 'No topic entered';
			} else {
				var newTop = {
					room: $scope.currentRoom,
					topic: $scope.msgtext
				};
				socket.emit('settopic', newTop, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to set topic';
					} else {
						$scope.msgtext = '';
					}
				});
			}
		};
		$scope.setPassword = function () {
			if ($scope.msgtext === '') {
				$scope.errorMessage = 'No password entered';
			} else {
				var newPass = {
					room: $scope.currentRoom,
					password: $scope.msgtext
				};
				socket.emit('setpassword', newPass, function (success) {
					if (!success) {
						$scope.errorMessage = 'Unable to set password';
					} else {
						$scope.msgtext = '';
					}
				});
			}
		};
		$scope.removePassword = function () {
			var remPass = {
				room: $scope.currentRoom
			};
			socket.emit('removepassword', remPass, function (success) {
				if (!success) {
					$scope.errorMessage = 'Unable to remove password';
				} else {
					$scope.msgtext = '';
				}
			});
		};
	}]);
}());