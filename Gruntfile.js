module.exports = function(grunt){
	require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		htmlhint: {
			build: {
				options: {
					'tag-pair': true,
		            'tagname-lowercase': true,
		            'attr-lowercase': true,
		            'attr-value-double-quotes': true,
		            'doctype-first': false,
		            'spec-char-escape': true,
		            'id-unique': true,
		            'head-script-disabled': false,
		            'style-disabled': true
				},
				src: ['index.html', 'views/*.html']
			}
		},
		jshint: {
			options: {
				reporter: require('jshint-stylish'),
				curly: true,
				immed: true,
				newcap: true,
				noarg: true,
				sub: true,
				boss: true,
				eqnull: true,
				node: true,
				undef: true,
				globals: {
					_: false,
					jQuery: false,
					angular: false,
					moment: false,
					console: false,
					$: false,
					io: false
				}
			},
			src: ['scripts/app.js', 'scripts/controllers/*.js'],
			gruntfile: ['Gruntfile.js'],
			server: {
				options: {
					'-W030': true,
					'-W079': true
				},
				src: ['chatserver.js']
			}
		},
		concat: {
			app: {
				src: ['scripts/app.js', 'scripts/controllers/*.js'],
				dest: 'scripts/build/app-min.js'
			}
		},
		uglify: {
			app: {
				src: 'scripts/build/app-min.js',
				dest: 'scripts/build/app-min.js'
			}
		},
		watch: {
			html: {
				files: ['index.html', 'views/*.html'],	
				tasks: ['htmlhint']
			},
			scripts: {
				files: ['scripts/app.js', 'scripts/controllers/*.js', 'Gruntfile.js'],
				tasks: ['jshint', 'concat', 'uglify']
			}
		}
	});
	grunt.registerTask("test", ['htmlhint', 'jshint']);
	grunt.registerTask("build", ['concat', 'uglify']);
	grunt.registerTask("default", ['htmlhint', 'jshint', 'concat', 'uglify']);
};